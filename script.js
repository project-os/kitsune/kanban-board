document.addEventListener('DOMContentLoaded', function() {
    const buttons = document.querySelectorAll('.control-panel button');

    buttons.forEach(button => {
        const title = button.getAttribute('title');
        button.setAttribute('data-title', title);
        button.removeAttribute('title');
    });

    // Dark mode toggle functionality
    const darkModeToggleButton = document.getElementById('darkModeToggle');
    const lightModeIcon = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12.85 2.48a1 1 0 0 1 .035.978A8 8 0 0 0 20 15.12h.004a1 1 0 0 1 .888 1.458A10 10 0 0 1 12 22C6.477 22 2 17.523 2 12c0-5.521 4.475-9.998 9.996-10a1 1 0 0 1 .854.48ZM10.45 4.15a8 8 0 1 0 7.82 12.82C13.57 16.15 10 12.054 10 7.12c0-1.033.157-2.03.449-2.97Z" fill="currentColor"/></svg>';
    const darkModeIcon = '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M12 15a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm0 2a5 5 0 1 0 0-10 5 5 0 0 0 0 10Z" fill="currentColor"/><path d="M11 3a1 1 0 1 1 2 0v1a1 1 0 1 1-2 0V3ZM5.636 7.05A1 1 0 0 0 7.05 5.638l-.707-.707A1 1 0 1 0 4.93 6.344l.707.707ZM19.071 4.93a1 1 0 0 0-1.414 0l-.707.707a1 1 0 0 0 1.414 1.414l.707-.707a1 1 0 0 0 0-1.414ZM7.05 16.95a1 1 0 0 0-1.414 0l-.707.706a1 1 0 1 0 1.414 1.415l.707-.708a1 1 0 0 0 0-1.414ZM19.071 19.07a1 1 0 0 1-1.414 0l-.707-.707a1 1 0 0 1 1.414-1.414l.707.707a1 1 0 0 1 0 1.414ZM12 19a1 1 0 0 0-1 1v1a1 1 0 1 0 2 0v-1a1 1 0 0 0-1-1ZM21 11a1 1 0 1 1 0 2h-1a1 1 0 1 1 0-2h1ZM5 12a1 1 0 0 0-1-1H3a1 1 0 1 0 0 2h1a1 1 0 0 0 1-1Z" fill="currentColor"/></svg>';

    darkModeToggleButton.addEventListener('click', function() {
        document.body.classList.toggle('dark-mode');
        if (document.body.classList.contains('dark-mode')) {
            darkModeToggleButton.innerHTML = darkModeIcon;
        } else {
            darkModeToggleButton.innerHTML = lightModeIcon;
        }
    });

    // Function to calculate and set the control panel height based on button count
    const setControlPanelHeight = () => {
        const panels = [
            { id: 'mainControlPanel', buttonHeight: 48, padding: 0 },
            { id: 'settingsPanel', buttonHeight: 47, padding: 0 }
        ];

        panels.forEach(panelConfig => {
            const panel = document.getElementById(panelConfig.id);
            const buttons = panel.querySelectorAll('button');
            const newHeight = (buttons.length * panelConfig.buttonHeight) + panelConfig.padding;
            if (panel.classList.contains('expanded')) {
                panel.style.height = `${newHeight}px`;
            } else {
                panel.style.height = '40px';
            }
        });
    };

    // Toggle function for a specific panel
    const togglePanel = (panel, buttons, icon) => {
        const addHoverEvents = (element) => {
            element.addEventListener('mouseover', function() {
                if (!panel.classList.contains('expanded')) {
                    panel.classList.add('expanded');
                    setControlPanelHeight(panel, buttons);
                    buttons.forEach(button => {
                        button.style.display = 'block';
                    });
                    panel.style.transition = 'height 0.3s ease';
                }
            });

            element.addEventListener('mouseout', function() {
                if (panel.classList.contains('expanded')) {
                    panel.classList.remove('expanded');
                    panel.style.height = '40px';
                    buttons.forEach(button => {
                        button.style.display = 'none';
                    });
                    panel.style.transition = 'height 0.3s ease';
                }
            });
        };

        addHoverEvents(panel);
        addHoverEvents(icon);
        buttons.forEach(button => addHoverEvents(button));
    };

    // Control panel toggle functionality
    const mainControlPanel = document.getElementById('mainControlPanel');
    const mainControlPanelButtons = mainControlPanel.querySelectorAll('button:not(#toggleControlPanelButton)');
    const mainToggleIcon = document.getElementById('toggleControlPanelButton');

    const settingsPanel = document.getElementById('settingsPanel');
    const settingsPanelButtons = settingsPanel.querySelectorAll('button:not(#settingsButton)');
    const settingsToggleIcon = document.getElementById('settingsButton');

    togglePanel(mainControlPanel, mainControlPanelButtons, mainToggleIcon);
    togglePanel(settingsPanel, settingsPanelButtons, settingsToggleIcon)

    const projectsButton = document.getElementById('projectsButton');
    const projectsTray = document.querySelector('.projects-tray');
    const kanbanBoard = document.querySelector('.kanban-board');

    projectsButton.addEventListener('click', function() {
        projectsTray.classList.toggle('visible');
        kanbanBoard.classList.toggle('shift-right');
    });


    // Function to add a new task
    function addTask(columnId, taskContent) {
        let column = document.getElementById(columnId);
        let newTask = document.createElement('div');
        newTask.classList.add('kanban-card');
        newTask.setAttribute('draggable', true);

        // Escape arbitrary empty space inputs
        if (/^\s*$/.test(taskContent)) {
            return;
        }

        let taskText = document.createElement('span');
        taskText.textContent = taskContent;
        newTask.appendChild(taskText);

        let buttonPanel = document.createElement('div');
        buttonPanel.classList.add('button-panel');
        buttonPanel.style.display = 'none'; // Initially hidden

        let deleteButton = document.createElement('panel-button');
        deleteButton.innerHTML = '<svg width="24" height="24" viewBox="0 0 24 24" fill=currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4 8h16v9a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V8Zm2 2v7a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-7H6Z" fill="currentColor"/><path d="M9 13a1 1 0 0 1 1-1h4a1 1 0 1 1 0 2h-4a1 1 0 0 1-1-1Z" fill="currentColor"/><path fill-rule="evenodd" clip-rule="evenodd" d="M3 5.5A2.5 2.5 0 0 1 5.5 3h13A2.5 2.5 0 0 1 21 5.5v2a2.5 2.5 0 0 1-2.5 2.5h-13A2.5 2.5 0 0 1 3 7.5v-2ZM5.5 5a.5.5 0 0 0-.5.5v2a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-2a.5.5 0 0 0-.5-.5h-13Z" fill="currentColor"/></svg>';
        deleteButton.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent event propagation
            let currentColumn = newTask.parentNode;
            currentColumn.removeChild(newTask);
        });
        buttonPanel.appendChild(deleteButton);

        let editButton = document.createElement('panel-button');
        editButton.innerHTML = '<svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M4 21a1 1 0 0 1 1-1h14a1 1 0 1 1 0 2H5a1 1 0 0 1-1-1Z" fill="currentColor"/><path fill-rule="evenodd" clip-rule="evenodd" d="M16.654 4.588a2 2 0 0 0-2.828 0L6.92 11.494l-.918 4.504 4.503-.918 6.907-6.906a2 2 0 0 0 0-2.828l-.758-.758Zm-4.242-1.414a4 4 0 0 1 5.656 0l.758.757a4 4 0 0 1 0 5.657l-6.906 6.906a2 2 0 0 1-1.015.546l-4.504.918a2 2 0 0 1-2.359-2.36l.918-4.503a2 2 0 0 1 .545-1.015l6.907-6.906Z" fill="currentColor"/></svg>';
        editButton.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent event propagation
            let newContent = prompt("Edit task:", taskText.textContent);
            if (newContent !== null) {
                taskText.textContent = newContent;
            }
        });
        buttonPanel.appendChild(editButton);

        // Append the button panel to the newTask
        newTask.appendChild(buttonPanel);

        // Add click event to show/hide the button panel
        newTask.addEventListener('click', function() {
            if (buttonPanel.style.display === 'none') {
                buttonPanel.style.display = 'flex';
            } else {
                buttonPanel.style.display = 'none';
            }
        });

        addDragListeners(newTask);
        column.appendChild(newTask);

        // Reset Input Field, Button Opacity, and Character Counter for the column
        let inputField = column.querySelector('.input-group input');
        inputField.value = '';
        let addButton = column.querySelector('.input-group button');
        if (addButton && addButton.tagName === 'BUTTON') {
            addButton.style.opacity = '0';
        }

        let charCounter = column.querySelector('.input-group span');
        if (charCounter) {
            charCounter.textContent = '0/169';
        }
    }


    // dynamically display the rename button 
    document.querySelectorAll('.input-group input').forEach(input => {
        input.addEventListener('input', function() {
            const button = this.nextElementSibling;
            if (this.value.trim() !== '') {
                button.style.opacity = '1';
            } else {
                button.style.opacity = '0';
            }
        });
    });
    
    // Function to rename columns
    document.querySelectorAll('.rename-column-button').forEach(button => {
        button.addEventListener('click', function() {
            // Find the column header
            let header = this.previousElementSibling;

            // Prompt for a new name
            let newName = prompt('Enter a new name for the column:', header.textContent);

            // Update the column header if a new name is provided
            if (newName && newName.trim() !== '') {
                header.textContent = newName.trim();
            }
        });
    });

    // Function to add drag listeners
    function addDragListeners(task) {
        task.addEventListener('dragstart', function() {
            draggedItem = task;
            task.classList.add('dragging');
            setTimeout(() => task.style.display = 'none', 0);
        });

        task.addEventListener('dragend', function() {
            setTimeout(() => {
                task.style.display = 'block';
                task.classList.remove('dragging');
                draggedItem = null;
                removeDropIndicators();
            }, 0);
        });
    }

    // Initialize drag and drop
    let draggedItem = null;
    let dropIndicator = document.createElement('div');
    dropIndicator.classList.add('drop-indicator');

    document.querySelectorAll('.kanban-card').forEach(addDragListeners);

    document.querySelectorAll('.kanban-column').forEach(column => {
        column.addEventListener('dragover', function(e) {
            e.preventDefault();
            const afterElement = getDragAfterElement(column, e.clientY);
            if (afterElement) {
                column.insertBefore(dropIndicator, afterElement);
            } else {
                column.appendChild(dropIndicator);
            }
        });

        column.addEventListener('dragleave', function() {
            if (column.contains(dropIndicator)) {
                column.removeChild(dropIndicator);
            }
        });

        column.addEventListener('drop', function(e) {
            e.preventDefault();
            if (draggedItem) {
                if (column.contains(dropIndicator)) {
                    column.insertBefore(draggedItem, dropIndicator);
                } else {
                    column.appendChild(draggedItem);
                }
                column.removeChild(dropIndicator)
            }
        });
    });

    function getDragAfterElement(container, y) {
        const draggableElements = [...container.querySelectorAll('.kanban-card:not(.dragging)')];

        return draggableElements.reduce((closest, child) => {
            const box = child.getBoundingClientRect();
            const offset = y - box.top - box.height / 2;
            if (offset < 0 && offset > closest.offset) {
                return { offset: offset, element: child };
            } else {
                return closest;
            }
        }, { offset: Number.NEGATIVE_INFINITY }).element;
    }

    // Function to remove drop indicators from all columns
    function removeDropIndicators() {
        document.querySelectorAll('.kanban-column').forEach(column => {
            if (column.contains(dropIndicator)) {
                column.removeChild(dropIndicator);
            }
        });
    }
    
function setupDragAndDropForColumn(column) {
    column.addEventListener('dragover', function(e) {
        e.preventDefault();
        const afterElement = getDragAfterElement(column, e.clientY);
        if (afterElement) {
            column.insertBefore(dropIndicator, afterElement);
        } else {
            column.appendChild(dropIndicator);
        }
    });

    column.addEventListener('dragleave', function() {
        if (column.contains(dropIndicator)) {
            column.removeChild(dropIndicator);
        }
    });

    column.addEventListener('drop', function(e) {
        e.preventDefault();
        if (draggedItem) {
            if (column.contains(dropIndicator)) {
                column.insertBefore(draggedItem, dropIndicator);
            } else {
                column.appendChild(draggedItem);
            }
            column.removeChild(dropIndicator);
        }
    });
}
    
function setupColumnListeners(column) {
    // Rename Listener
    let renameButton = column.querySelector('.rename-column-button');
    renameButton.addEventListener('click', function() {
        let header = this.parentNode.querySelector('h2');
        let newName = prompt('Enter a new name for the column:', header.textContent);
        if (newName && newName.trim() !== '') {
            header.textContent = newName.trim();
        }
    });

    // Add Task Listener
    let input = column.querySelector('.input-group input');
    let addButton = column.querySelector('.input-group button');
    let charCounter = column.querySelector('.input-group span'); // Get the character counter

    addButton.addEventListener('click', function() {
        if (input.value.trim() !== '') {
            addTask(column.id, input.value.trim());
            input.value = '';
            addButton.style.opacity = '0';
            charCounter.textContent = '0/169'; // Reset character counter on button click
        }
    });

    // Input Event for Button Visibility and Character Count
    input.addEventListener('input', function() {
        const charCount = this.value.length;
        charCounter.textContent = `${charCount}/169`; // Update character counter
        addButton.style.opacity = this.value.trim() !== '' ? '1' : '0';
    });

    // Enter Key Listener for Adding Task
    input.addEventListener('keydown', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            if (input.value.trim() !== '') {
                addTask(column.id, input.value.trim());
                input.value = '';
                addButton.style.opacity = '0';
                charCounter.textContent = '0/169';
                charCounter.style.color = 'grey'; // Manually reset the color to grey
            }
        }
    });
}
       

// Function to add a task to a specific column
function addTaskToColumn(taskInputId, columnId) {
    let taskContent = document.getElementById(taskInputId).value;
    if (taskContent) {
        addTask(columnId, taskContent);
        document.getElementById(taskInputId).value = '';

        // Reset button opacity and character counter
        let inputField = document.getElementById(taskInputId);
        let addButton = inputField.nextElementSibling; // Assuming the button is right next to the input

        // Ensure that we are only changing the opacity of the button, not any other element
        if (addButton.tagName === 'BUTTON') {
            addButton.style.opacity = '0';
        }
    }
}



    // hardcoded until setup page created
    // Event listeners for each add task button
    document.getElementById('addTaskButtonTodo').addEventListener('click', function() {
        addTaskToColumn('newTaskContentTodo', 'todo');
    });

    document.getElementById('addTaskButtonInProgress').addEventListener('click', function() {
        addTaskToColumn('newTaskContentInProgress', 'in-progress');
    });

    document.getElementById('addTaskButtonDone').addEventListener('click', function() {
        addTaskToColumn('newTaskContentDone', 'done');
    });


// Reusable function to add task based on input field and column ID
function handleTaskAddition(inputId, columnId) {
    const inputField = document.getElementById(inputId);
    inputField.addEventListener('keydown', function(event) {
        if (event.key === 'Enter') {
            event.preventDefault(); // Prevent the default action to avoid form submission

            if (inputField.value.trim() !== '') {
                addTaskToColumn(inputId, columnId);

                // Reset the input field and character counter after adding the task
                inputField.value = '';
                const charCounter = document.getElementById(columnId + '-char-counter');
                if (charCounter) {
                    charCounter.textContent = '0/169';
                    updateCharCounter(inputField, charCounter); // Update the character counter color
                }
            }
        }
    });
}

    // hardcoded until setup page created
    // Add event listener to each input field for the Enter key
    handleTaskAddition('newTaskContentTodo', 'todo');
    handleTaskAddition('newTaskContentInProgress', 'in-progress');
    handleTaskAddition('newTaskContentDone', 'done');
    
    document.getElementById('addColumnButton').addEventListener('click', function() {
        addColumn();
    });
    //document.getElementById('renameColumnButton').addEventListener('click', renameColumn);
    //document.getElementById('shareBoardButton').addEventListener('click', shareBoard);
    document.getElementById('saveBoardButton').addEventListener('click', saveBoard);
    document.getElementById('openBoardButton').addEventListener('click', openBoard);

// Helper function to update character counter and change color
function updateCharCounter(input, charCounter) {
    const charCount = input.value.length;
    charCounter.textContent = `${charCount}/169`;

    if (169 - charCount <= 6) {
        charCounter.style.color = '#F40009'; // Red color for last 6 characters
    } else {
        charCounter.style.color = 'grey'; // Default color
    }
}

function addColumn(columnName) {
    var newColumn = document.createElement('div');
    newColumn.className = 'kanban-column';

    var headerGroup = document.createElement('div');
    headerGroup.className = 'header-group';

    var header = document.createElement('h2');
    header.textContent = columnName || prompt("Enter column name:", "New Column");
    headerGroup.appendChild(header);

    // rename button dynamically added to new columns upon creation
    var renameButton = document.createElement('button');
    renameButton.className = 'rename-column-button';
    renameButton.title = 'Rename Button';
    renameButton.innerHTML = '<svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path d="M4 21a1 1 0 0 1 1-1h14a1 1 0 1 1 0 2H5a1 1 0 0 1-1-1Z" fill="currentColor"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12.412 3.174a4 4 0 0 1 5.657 0l.757.758a4 4 0 0 1 0 5.657l-6.906 6.906a2 2 0 0 1-1.015.545l-4.503.918a2 2 0 0 1-2.36-2.359l.918-4.504a2 2 0 0 1 .546-1.014l6.906-6.907Zm4.242 1.415a2 2 0 0 0-2.828 0L13.414 5 17 8.586l.412-.412a2 2 0 0 0 0-2.828l-.758-.757ZM8.414 10 12 6.415 15.586 10 12 13.586 8.414 10ZM7 11.415l-.08.08-.918 4.504 4.504-.918.08-.08L7 11.414Z" fill="currentColor"/></svg>';
    headerGroup.appendChild(renameButton);

    newColumn.appendChild(headerGroup);

    var columnId = 'column-' + Date.now();
    newColumn.id = columnId;

    var inputGroup = document.createElement('div');
    inputGroup.className = 'input-group';

    var input = document.createElement('input');
    input.type = 'text';
    input.placeholder = 'Create task';
    input.maxLength = 169;
    inputGroup.appendChild(input);

    var charCounter = document.createElement('span');
    charCounter.id = columnId + '-char-counter';
    charCounter.textContent = '0/169';
    charCounter.className = 'invisible'; // Initially set to invisible
    inputGroup.appendChild(charCounter);

    // Event listener to make charCounter visible when input is in focus
    input.addEventListener('focus', function() {
        charCounter.classList.remove('invisible');
    });

    // Event listener to conditionally hide charCounter when input loses focus
    input.addEventListener('blur', function() {
        // Hide charCounter only if input field is empty
        if (input.value.trim() === '') {
            charCounter.classList.add('invisible');
        }
    });

    // Event listener for input changes to update visibility based on content
    input.addEventListener('input', function() {
        if (input.value.trim() === '') {
            charCounter.classList.add('invisible');
        } else {
            charCounter.classList.remove('invisible');
        }
    });


    var addButton = document.createElement('button');
    addButton.textContent = '+';
    addButton.addEventListener('click', function() {
        if (input.value.trim() !== '') {
            addTask(columnId, input.value.trim());
            input.value = '';
            charCounter.textContent = '0/169';
        }
    });
    inputGroup.appendChild(addButton);
    
    input.addEventListener('input', function() {
        updateCharCounter(input, charCounter); // Update color on input
    });

    newColumn.appendChild(inputGroup);
    var boardContainer = document.querySelector('.kanban-board');
    boardContainer.appendChild(newColumn);
    setupDragAndDropForColumn(newColumn);
    setupColumnListeners(newColumn);
    
    setTimeout(() => {
        boardContainer.scrollTo({
            left: boardContainer.scrollWidth,
            behavior: 'smooth'
        });
    }, 100);
}


//function shareBoard() {
    // Implementation to share the board with other users
//}

    function saveBoard() {
        var boardData = [];

        // Iterate through each column and its tasks
        document.querySelectorAll('.kanban-column').forEach(column => {
            var columnData = { 
                name: column.querySelector('h2').textContent,
                tasks: []
            };

            // Iterate through task cards targeting span to ensure button text not included
            column.querySelectorAll('.kanban-card').forEach(task => {
                var taskText = task.querySelector('span').textContent;
                columnData.tasks.push(taskText);
            });

            boardData.push(columnData);
        });

        // Save the board data as a JSON string in local storage
        localStorage.setItem('kanbanBoardData', JSON.stringify(boardData));
        
            // Convert boardData to JSON string
        var json = JSON.stringify(boardData);

        // Get current date and time in YYYYMMDD_HHMMSS format
        var now = new Date();
        var dateStr = ('0' + now.getDate()).slice(-2) + '-' + ('0' + (now.getMonth() + 1)).slice(-2) + '-' + now.getFullYear();

        // Create a blob with the JSON data
        var blob = new Blob([json], {type: "application/json"});

        // Create a link element to download the blob
        var a = document.createElement('a');
        a.href = URL.createObjectURL(blob);
        a.download = 'kanban_' + dateStr + '.json';

        // Append the link, trigger the download, then remove the link
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }


    function rebuildBoard(boardData) {
        var board = document.querySelector('.kanban-board');
        board.innerHTML = '';

        // Create a map of column names to DOM elements
        var columnsMap = {};

        boardData.forEach(columnData => {
            addColumn(columnData.name);
            var newColumn = board.lastChild;
            columnsMap[columnData.name] = newColumn;

            if (columnData.tasks && columnData.tasks.length > 0) {
                // Reverse the tasks array to maintain the correct order
                columnData.tasks.slice().reverse().forEach(taskText => {
                    addTask(columnsMap[columnData.name].id, taskText);
                });
            }
        });
    }

    function openBoard() {
        var fileInput = document.createElement('input');
        fileInput.type = 'file';
        fileInput.accept = '.json';
        fileInput.onchange = e => {
            var file = e.target.files[0];
            if (!file) return;

            var reader = new FileReader();
            reader.onload = e => {
                var boardData = JSON.parse(e.target.result);
                rebuildBoard(boardData);
            };
            reader.readAsText(file);
        };
        fileInput.click();
    }







    const changeAccentButton = document.getElementById('changeAccentButton');
    const colorPicker = document.createElement('input');
    colorPicker.type = 'color';
    colorPicker.style.display = 'none';

    colorPicker.addEventListener('input', function() {
        const selectedColor = colorPicker.value;
        document.documentElement.style.setProperty('--accent-color', selectedColor);
    });

    changeAccentButton.addEventListener('click', function() {
        colorPicker.click();
    });

    document.body.appendChild(colorPicker);










    // Function to toggle project options
    function toggleProjectOptions(projectItem) {
        const projectOptions = projectItem.querySelector('.project-options-container');
        if (projectOptions.style.display === 'flex') {
            projectOptions.style.display = 'none';
        } else {
            projectOptions.style.display = 'flex';
        }
    }

    // Event listener for project items
    document.querySelectorAll('.project-item').forEach(item => {
        item.addEventListener('click', function(event) {
            event.stopPropagation(); // Prevent event from bubbling up
            toggleProjectOptions(item);
        });
    });

    // Function to create a new project item
    function createProjectItem(title) {
        const projectItem = document.createElement('div');
        projectItem.className = 'project-item';

        const projectHeader = document.createElement('div');
        projectHeader.className = 'project-header';

        const projectTitle = document.createElement('span');
        projectTitle.className = 'project-title';
        projectTitle.textContent = title;

        projectHeader.appendChild(projectTitle);

        const boardList = document.createElement('div');
        boardList.className = 'board-list';
        boardList.innerHTML = `
            <div class="board-item">Board 1</div>
        `;

        const projectOptionsContainer = document.createElement('div');
        projectOptionsContainer.className = 'project-options-container';
        projectOptionsContainer.innerHTML = `
            <button class="project-option" title="Archive Project">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M4 8h16v9a4 4 0 0 1-4 4H8a4 4 0 0 1-4-4V8Zm2 2v7a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2v-7H6Z" fill="currentColor"/>
                    <path d="M9 13a1 1 0 0 1 1-1h4a1 1 0 1 1 0 2h-4a1 1 0 0 1-1-1Z" fill="currentColor"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3 5.5A2.5 2.5 0 0 1 5.5 3h13A2.5 2.5 0 0 1 21 5.5v2a2.5 2.5 0 0 1-2.5 2.5h-13A2.5 2.5 0 0 1 3 7.5v-2ZM5.5 5a.5.5 0 0 0-.5.5v2a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-2a.5.5 0 0 0-.5-.5h-13Z" fill="currentColor"/>
                </svg>
            </button>
            <button class="project-option" title="Rename Project">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path d="M4 21a1 1 0 0 1 1-1h14a1 1 0 1 1 0 2H5a1 1 0 0 1-1-1Z" fill="currentColor"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M16.654 4.588a2 2 0 0 0-2.828 0L6.92 11.494l-.918 4.504 4.503-.918 6.907-6.906a2 2 0 0 0 0-2.828l-.758-.758Zm-4.242-1.414a4 4 0 0 1 5.656 0l.758.757a4 4 0 0 1 0 5.657l-6.906 6.906a2 2 0 0 1-1.015.546l-4.504.918a2 2 0 0 1-2.359-2.36l.918-4.503a2 2 0 0 1 .545-1.015l6.907-6.906Z" fill="currentColor"/>
                </svg>
            </button>
            <button class="project-option" title="New Board">
                <svg width="24" height="24" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 4a1 1 0 0 0-1 1v6H5a1 1 0 1 0 0 2h6v6a1 1 0 1 0 2 0v-6h6a1 1 0 1 0 0-2h-6V5a1 1 0 0 0-1-1Z" fill="currentColor"/>
                </svg>
            </button>
            <button class="project-option" title="Project Settings">
                <svg id="settingsIcon" width="24" height="24" viewBox="0 0 24 24" fill-rule="evenodd" clip-rule="evenodd" xmlns="http://www.w3.org/2000/svg"><path d="M12.5 2a2.492 2.492 0 0 1 1.818.784c.4.423.653.986.68 1.607v.021c.002.03.002.059.002.088v.014a.1.1 0 0 0 .172.071l.01-.01.063-.06.016-.015a2.49 2.49 0 0 1 1.691-.657c.64 0 1.278.245 1.765.732l.708.707a2.492 2.492 0 0 1 .73 1.84 2.49 2.49 0 0 1-.655 1.617l-.014.016-.061.063-.01.01a.1.1 0 0 0 .07.172h.015c.03 0 .059 0 .088.002h.02a2.49 2.49 0 0 1 1.662.732c.451.452.73 1.077.73 1.766v1a2.492 2.492 0 0 1-.784 1.818c-.423.4-.986.653-1.607.68h-.021c-.03.002-.059.002-.088.002h-.014a.1.1 0 0 0-.071.172l.01.01.06.063.015.016a2.49 2.49 0 0 1 .657 1.691c0 .64-.245 1.278-.732 1.765l-.707.708a2.492 2.492 0 0 1-1.84.73 2.49 2.49 0 0 1-1.617-.655l-.016-.014a2.59 2.59 0 0 1-.063-.061l-.01-.01a.1.1 0 0 0-.172.07v.015c0 .03 0 .059-.002.088v.02a2.491 2.491 0 0 1-.732 1.662A2.492 2.492 0 0 1 12.5 22h-1a2.492 2.492 0 0 1-1.818-.784 2.49 2.49 0 0 1-.68-1.607v-.021A2.538 2.538 0 0 1 9 19.5v-.014a.1.1 0 0 0-.172-.071l-.01.01-.063.06-.016.015a2.49 2.49 0 0 1-1.691.657 2.492 2.492 0 0 1-1.766-.732l-.707-.707a2.492 2.492 0 0 1-.73-1.84 2.49 2.49 0 0 1 .655-1.617l.014-.016.061-.063.01-.01a.1.1 0 0 0-.07-.172H4.5c-.03 0-.059 0-.088-.002h-.02a2.492 2.492 0 0 1-1.662-.732A2.492 2.492 0 0 1 2 12.5v-1a2.492 2.492 0 0 1 .784-1.818c.423-.4.986-.653 1.607-.68h.021L4.5 9h.014a.1.1 0 0 0 .071-.172l-.01-.01a2.558 2.558 0 0 1-.06-.063L4.5 8.739a2.49 2.49 0 0 1-.657-1.691c0-.64.245-1.278.732-1.766l.707-.707a2.492 2.492 0 0 1 1.84-.73 2.49 2.49 0 0 1 1.617.655l.016.014.063.061.01.01A.1.1 0 0 0 9 4.515V4.5c0-.03 0-.059.002-.088v-.02a2.491 2.491 0 0 1 .732-1.662A2.492 2.492 0 0 1 11.5 2h1ZM11 19.5a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5v-.014A2.1 2.1 0 0 1 16.586 18l.01.01a.5.5 0 0 0 .707 0l.707-.707a.5.5 0 0 0 0-.707l-.01-.01A2.1 2.1 0 0 1 19.486 13h.014a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5h-.014A2.1 2.1 0 0 1 18 7.414l.01-.01a.5.5 0 0 0 0-.707l-.707-.707a.5.5 0 0 0-.707 0l-.01.01A2.1 2.1 0 0 1 13 4.514V4.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v.014A2.1 2.1 0 0 1 7.414 6l-.01-.01a.5.5 0 0 0-.707 0l-.707.707a.5.5 0 0 0 0 .707l.01.01A2.1 2.1 0 0 1 4.514 11H4.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h.014A2.1 2.1 0 0 1 6 16.586l-.01.01a.5.5 0 0 0 0 .707l.707.707a.5.5 0 0 0 .707 0l.01-.01A2.1 2.1 0 0 1 11 19.486v.014Z" fill="currentColor"/>
                    <path d="M12 14a2 2 0 1 0 0-4 2 2 0 0 0 0 4Zm0 2a4 4 0 1 0 0-8 4 4 0 0 0 0 8Z" fill="currentColor"/>
                </svg>
            </button>
        `;

        projectOptionsContainer.style.display = 'none'; // Initially hidden

        projectItem.appendChild(projectHeader);
        projectItem.appendChild(boardList);
        projectItem.appendChild(projectOptionsContainer);

        // Add click event to project item to show/hide the project options
        projectItem.addEventListener('click', function(event) {
            event.stopPropagation();
            toggleProjectOptions(projectItem);
        });

        return projectItem;
    }

    // Create initial project item
    const initialProjectItem = createProjectItem('Project 1');
    document.querySelector('.project-list').appendChild(initialProjectItem);

    // Event listener for create project button
    const createProjectButton = document.getElementById('createProjectButton');
    createProjectButton.addEventListener('click', function() {
        const newProjectTitle = prompt('Enter new project name:', 'New Project');
        if (newProjectTitle && newProjectTitle.trim() !== '') {
            const newProjectItem = createProjectItem(newProjectTitle.trim());
            document.querySelector('.project-list').appendChild(newProjectItem);
        }
    });

    // Function to handle profile image upload
    function handleProfileImageUpload(event) {
        const userProfileImage = document.getElementById('userProfileImage');
        const file = event.target.files[0];
        if (file) {
            const reader = new FileReader();
            reader.onload = function(e) {
                userProfileImage.src = e.target.result;
                userProfileImage.style.display = 'block'; // Show the uploaded image
                userProfile.querySelector('svg').style.display = 'none'; // Hide the placeholder SVG
            };
            reader.readAsDataURL(file);
        }
    }

    // Event listener for user profile click
    const userProfile = document.getElementById('userProfile');
    userProfile.addEventListener('click', function() {
        const fileInput = document.createElement('input');
        fileInput.type = 'file';
        fileInput.accept = 'image/*';
        fileInput.addEventListener('change', handleProfileImageUpload);
        fileInput.click();
    });

});

