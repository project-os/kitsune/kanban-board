let availableGradients = Array.from({ length: 15 }, (_, i) => i); // Assuming 15 gradient pairs
let usedGradients = [];
let lastNoiseTexture = null; // To store the last noise texture
let lastGrainSize = null; // To store the last grain size
let lastNoiseOpacity = null; // To store the last noise opacity

function getRandomGradientIndex() {
    if (availableGradients.length === 0) {
        availableGradients = usedGradients;
        usedGradients = [];
    }

    const randomIndex = Math.floor(Math.random() * availableGradients.length);
    const selectedGradient = availableGradients[randomIndex];

    availableGradients.splice(randomIndex, 1);
    usedGradients.push(selectedGradient);

    return selectedGradient;
}

function animateSlider(sliderId, targetValue, duration) {
    return new Promise((resolve) => {
        const slider = document.getElementById(sliderId);
        if (!slider) {
            resolve();
            return;
        }

        const startValue = parseFloat(slider.value);
        const startTime = performance.now();

        function easeOutCubic(t) {
            return --t * t * t + 1;
        }

        function animate(time) {
            let timeFraction = (time - startTime) / duration;
            if (timeFraction > 1) timeFraction = 1;

            // Apply the easing function to timeFraction
            const easedTimeFraction = easeOutCubic(timeFraction);
            const currentValue = startValue + (targetValue - startValue) * easedTimeFraction;
            slider.value = currentValue;

            if (timeFraction < 1) {
                requestAnimationFrame(animate);
            } else {
                resolve(); // Resolve the promise when animation is complete
            }
        }

        requestAnimationFrame(animate);
    });
}

function applyGradient(gradientIndex) { // magic random button
    const gradientStart = getComputedStyle(document.documentElement).getPropertyValue(`--gradient-${gradientIndex}-start`);
    const gradientEnd = getComputedStyle(document.documentElement).getPropertyValue(`--gradient-${gradientIndex}-end`);

    console.log(`Applying gradient: Start = ${gradientStart}, End = ${gradientEnd}`);

    document.documentElement.style.setProperty('--current-gradient-start', gradientStart);
    document.documentElement.style.setProperty('--current-gradient-end', gradientEnd);

    // Update the color input values
    document.getElementById('colorA').value = gradientStart;
    document.getElementById('colorB').value = gradientEnd;

    // Set a slight delay before updating background colors
    setTimeout(() => {
        const customColorPickerA = document.querySelector('.custom-color-picker[onclick*="colorA"]');
        if (customColorPickerA) {
            customColorPickerA.style.backgroundColor = gradientStart;
        }
        const customColorPickerB = document.querySelector('.custom-color-picker[onclick*="colorB"]');
        if (customColorPickerB) {
            customColorPickerB.style.backgroundColor = gradientEnd;
        }
    }, 100); // 100ms delay to trigger CSS transition effect

    // Update button colors
    const button = document.querySelector('.control-panel .custom-button');
    button.style.backgroundColor = gradientStart;
    button.onmouseenter = () => button.style.backgroundColor = gradientEnd;
    button.onmouseleave = () => button.style.backgroundColor = gradientStart;

    // Randomize noise opacity and grain size
    const randomOpacity = Math.random() * (0.3 - 0.05) + 0.05;
    const randomGrainSize = Math.floor(Math.random() * (100 - 1) + 5);

    Promise.all([
        animateSlider('noiseOpacity', randomOpacity, 300),
        animateSlider('grainSize', randomGrainSize, 300)
    ]).then(() => {
        applyNoise(); // Call applyNoise after both animations complete
    });

    // Randomize noise on/off state
    const noiseToggle = document.getElementById('noiseToggle');
    const blurToggle = document.getElementById('blurToggle');
    const randomNoiseState = Math.random() < 0.75; // 75% chance

    // Apply noise and determine blur state based on noise state
    if (randomNoiseState) {
        // If noise is ON, decide randomly whether to apply blur
        const randomBlurState = Math.random() < 0.5; // .75 * .5 = .325 chance
        blurToggle.checked = randomBlurState; // Update blur toggle state
        
        // Animate sliders with specified delays
        setTimeout(() => {
            animateSlider('noiseOpacity', randomOpacity, 350);
        }, 100);

        setTimeout(() => {
            animateSlider('grainSize', randomGrainSize, 400);
        }, 150);

        applyNoise();

        // Apply or remove blur based on the random state
        if (randomBlurState) {
            const randomIntensity = Math.random() * (10 - 0.5) + 0.5; // Random between 0.5 and 10
            setTimeout(() => {
                animateSlider('blurIntensity', randomIntensity, 425);
            }, 175);
            applyBlurEffect();
        } else {
            removeBlurEffect();
        }
    } else {
        // If noise is OFF, turn off blur as well
        blurToggle.checked = false;
        removeBlurEffect();
    }

    updateToggleColors();
    updateSliderThumbColors();
}

function removeBlurEffect() {
    const targetElement = document.querySelector('.noise-overlay');
    if (targetElement) {
        // Introduce a delay before removing the blur effect
        setTimeout(() => {
            targetElement.style.filter = 'none';
        }, 400);
    }
}


function updateGradient() { // manual colour pickers
    const colorA = document.getElementById('colorA').value;
    const colorB = document.getElementById('colorB').value;

    console.log(`Manual gradient update: Color A = ${colorA}, Color B = ${colorB}`); // Log manual gradient update

    document.documentElement.style.setProperty('--current-gradient-start', colorA);
    document.documentElement.style.setProperty('--current-gradient-end', colorB);
    
    applyNoise();
    updateToggleColors();
    updateSliderThumbColors();

    // Update custom color picker for Color A
    const customColorPickerA = document.querySelector('.custom-color-picker[onclick*="colorA"]');
    if (customColorPickerA) {
        customColorPickerA.style.backgroundColor = colorA;
    }

    // Update custom color picker for Color B
    const customColorPickerB = document.querySelector('.custom-color-picker[onclick*="colorB"]');
    if (customColorPickerB) {
        customColorPickerB.style.backgroundColor = colorB;
    }

    // Update button colors based on the manually picked colors
    const button = document.querySelector('.control-panel .custom-button');
    button.style.backgroundColor = colorA; // On color
    button.onmouseenter = () => button.style.backgroundColor = colorB; // Off color on hover
    button.onmouseleave = () => button.style.backgroundColor = colorA;
}

function generateNoise(canvas, width, height, grainSize, opacity) {
    let ctx = canvas.getContext('2d');
    canvas.width = width;
    canvas.height = height;

    ctx.globalAlpha = opacity; // Set the global alpha based on noise opacity

    for (let i = 0; i < width; i += grainSize) {
        for (let j = 0; j < height; j += grainSize) {
            const grayScale = Math.random() * 255;
            ctx.fillStyle = `rgba(${grayScale}, ${grayScale}, ${grayScale})`;
            ctx.fillRect(i, j, grainSize, grainSize);
        }
    }
}

function applyNoise() {
    const noiseOverlay = document.querySelector('.noise-overlay');
    const noiseToggle = document.getElementById('noiseToggle');

    if (!noiseOverlay || !noiseToggle) {
        console.error('Noise overlay or toggle element not found!');
        return;
    }

    const grainSize = parseInt(document.getElementById('grainSize').value, 10);
    const noiseOpacity = parseFloat(document.getElementById('noiseOpacity').value);

    const shouldGenerateTexture = !lastNoiseTexture || grainSize !== lastGrainSize || noiseOpacity !== lastNoiseOpacity;
    if (shouldGenerateTexture) {
        const canvas = document.createElement('canvas');
        generateNoise(canvas, window.innerWidth, window.innerHeight, grainSize, noiseOpacity);

        noiseOverlay.style.transition = 'opacity 0.5s ease';
        noiseOverlay.style.opacity = '0';

        setTimeout(() => {
            lastNoiseTexture = canvas.toDataURL();
            lastGrainSize = grainSize;
            lastNoiseOpacity = noiseOpacity;

            noiseOverlay.style.backgroundImage = `url(${lastNoiseTexture})`;

            if (noiseToggle.checked) {
                // Wait for the fade-out to complete before starting the fade-in
                setTimeout(() => {
                    noiseOverlay.style.opacity = '1';
                }, 50); // Slight delay to ensure the fade-out completes
            }
        }, 450); // Wait for the old texture to fade out
    } else if (!noiseToggle.checked) {
        noiseOverlay.style.opacity = '0';
        setTimeout(() => {
            lastNoiseTexture = null;
            noiseOverlay.style.backgroundImage = 'none';
        }, 450);
    }

    noiseOverlay.style.visibility = noiseToggle.checked ? 'visible' : 'hidden';
}



function updateToggleColors() {
    const gradientStart = getComputedStyle(document.documentElement).getPropertyValue('--current-gradient-start');
    const gradientEnd = getComputedStyle(document.documentElement).getPropertyValue('--current-gradient-end');

    // Update noise toggle colors
    const noiseLayer = document.querySelector('#noise-toggle .layer');
    if (noiseLayer) {
        noiseLayer.style.backgroundColor = gradientEnd;
    }

    const noiseCheckbox = document.getElementById('noiseToggle');
    if (noiseCheckbox && noiseCheckbox.checked) {
        noiseLayer.style.backgroundColor = gradientStart;
    }

    // Update blur toggle colors
    const blurLayer = document.querySelector('#blur-toggle .layer');
    if (blurLayer) {
        blurLayer.style.backgroundColor = gradientEnd;
    }

    const blurCheckbox = document.getElementById('blurToggle');
    if (blurCheckbox && blurCheckbox.checked) {
        blurLayer.style.backgroundColor = gradientStart;
    }
}


function updateSliderThumbColors() {
    const gradientStart = getComputedStyle(document.documentElement).getPropertyValue('--current-gradient-start');
    const gradientEnd = getComputedStyle(document.documentElement).getPropertyValue('--current-gradient-end');

    const sliders = document.querySelectorAll('.slider input[type="range"]');
    sliders.forEach(slider => {
        // Set properties for thumb color
        slider.style.setProperty('--thumb-color', gradientStart);
        slider.style.setProperty('--thumb-hover-color', gradientEnd);

        // Set properties for slider bar color on hover
        slider.style.setProperty('--slider-bar-hover-color', gradientStart);
    });
}

function applyBlurEffect() {
    const blurToggle = document.getElementById('blurToggle');
    const blurIntensitySlider = document.getElementById('blurIntensity');
    const targetElement = document.querySelector('.noise-overlay'); // Adjust selector to target element for blur

    if (!targetElement) {
        console.error('Target element for blur not found!');
        return;
    }

    // Set the transition on the filter property
    targetElement.style.transition = 'filter 0.2s ease';

    if (blurToggle.checked) {
        const intensity = blurIntensitySlider.value;
        // Apply the blur effect with a fade-in transition
        setTimeout(() => {
            targetElement.style.filter = `blur(${intensity}px)`;
        }, 150); // 150ms delay for the fade-in effect
    } else {
        // Remove the blur effect immediately
        targetElement.style.filter = 'none';
    }
}


// Event listeners and initializations
window.addEventListener('load', () => {
    const noiseElement = document.querySelector('.noise');
    if (!noiseElement) {
        console.error('Noise element not found!');
        return;
    }

    // Create the noise overlay element if it doesn't exist
    let noiseOverlay = document.querySelector('.noise-overlay');
    if (!noiseOverlay) {
        noiseOverlay = document.createElement('div');
        noiseOverlay.classList.add('noise-overlay');
        noiseElement.appendChild(noiseOverlay);
    }

    // Initialize the first gradient
    applyGradient(getRandomGradientIndex());

    // Delayed call to applyNoise and updateToggleColors
    setTimeout(() => {
        applyNoise();
        applyBlurEffect();
        updateToggleColors();
    }, 50);
});

// Event listener for resizing
window.addEventListener('resize', () => {
    applyNoise();
    applyBlurEffect();
    updateToggleColors();
});

// Event listeners for toggles
document.getElementById('noiseToggle').addEventListener('change', function() {
    setTimeout(() => {
        applyNoise();
        updateToggleColors();
    }, 50);
});

document.getElementById('blurToggle').addEventListener('change', function() {
    setTimeout(() => {
        applyBlurEffect();
        updateToggleColors();
    }, 50);
});

// Event listeners for colour pickers, and magic button
document.getElementById('colorA').addEventListener('change', updateGradient);
document.getElementById('colorB').addEventListener('change', updateGradient);
document.getElementById('changeGradient').addEventListener('click', function() {
    applyGradient(getRandomGradientIndex());
});

// Event listeners for sliders
document.getElementById('noiseOpacity').addEventListener('input', applyNoise);
document.getElementById('grainSize').addEventListener('input', applyNoise);
document.getElementById('blurIntensity').addEventListener('input', applyBlurEffect);

